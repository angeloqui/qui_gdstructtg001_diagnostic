#include <iostream>
#include <time.h>

using namespace std;

void swapNum(int* num, int i, int j) {
	int temp = num[j];
	num[j] = num[i];
	num[i] = temp;
}

void sort(int* num, int order) {
	int temp;
	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)

	if (order == 1) {
		if (num[i] > num[j]) swapNum(num, i, j);
	} else {
		if (num[i] < num[j]) swapNum(num, i, j);
	}
}

void search(int* num) {
	int temp;
	cout << "Search a number: ";
	cin >> temp;

	for (int i = 0; i < 10; i++) {
		if (temp == num[i]) {
			cout << "Number found! Steps taken: " << i;
			break;
		}

		if (i == 9) cout << "Number is not inside the array";
	}
}

int main() {
	srand(time(NULL));

	//create var
	int num[10];

	//assign random values
	for (int i = 0; i < 10; i++) {
		num[i] = rand() % 69 + 1;
	}

	while (true) {
		
		//display array values
		for (int i = 0; i < 10; i++) cout << num[i] << " ";

		//UI
		int option;
		cout << "\n\n1.) Ascending" << "\n2.) Descending" << "\n3.) Search" << "\n\nSelect an option : ";
		cin >> option;

		if (option == 1) sort(num, 0);
		else if (option == 2) sort(num, 1);
		else search(num);

		cout << endl;
		system("pause");
		system("cls");
	}
	return 0;
}